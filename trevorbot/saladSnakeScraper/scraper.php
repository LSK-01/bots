<?php

set_time_limit(0);
date_default_timezone_set('UTC');
$folderPath = "/Users/luca/Desktop/bots/trevorbot/saladSnakeScraper";
require "/Users/luca/Desktop/bots/trevorbot/composer/vendor/autoload.php";

function imageDownload($imagePath, $mediaObj){
    $url = $mediaObj->getImageVersions2()->getCandidates()[0]->getUrl();
    $data = file_get_contents($url);
    file_put_contents($imagePath, $data);
  }

  function videoDownload($videoPath, $mediaObj){
    $url = $mediaObj->getVideoVersions()[0]->getUrl();
    $data = file_get_contents($url);
    file_put_contents($videoPath, $data);
  }

  //get a random file/folder
function randomMedia($path)
{
    $files = glob($path . '/*');
    $file = array_rand($files);
    return $files[$file];
}

function fetchCaption($path)
{
  $read = fopen($path, "r");
  $op = fread($read, filesize($path));
  fclose($read);
  return $op;
}

////// NORMALIZE MEDIA //////
// All album files must have the same aspect ratio.
// We copy the app's behavior by using the first file
// as template for all subsequent ones.
$mediaOptions = [
    'targetFeed' => \InstagramAPI\Constants::FEED_TIMELINE_ALBUM,
    // Uncomment to expand media instead of cropping it.
    'operation' => \InstagramAPI\Media\InstagramMedia::EXPAND,
];


/////// CONFIG ///////
$username = 'meem.scraper.6969';
$password = 'trevorbot4000';
$mediaIdArray = [];
$debug = false;
$truncatedDebug = false;
$accountBeingScraped = "583383672";
//////////////////////


$ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);
//for making sure video format/dimensions etc. are compatible to upload to insta
\InstagramAPI\Utils::$ffprobeBin = '/usr/local/bin/ffprobe';
\InstagramAPI\Media\Video\FFmpeg::$defaultBinary = '/usr/local/bin/ffmpeg';

try {
    $ig->login($username, $password);
} catch (\Exception $e) {
    echo 'Something went wrong: '.$e->getMessage()."\n";
    exit();
}


$feed = $ig->timeline->getUserFeed($accountBeingScraped);


$lastMediaIdScraped = file_get_contents("$folderPath/lastMediaIdScraped.txt");
$scrapedItems = $feed->getItems();

if (((end($scrapedItems))->getId()) == $lastMediaIdScraped){
    //ie he hasnt posted anything since our last scrape
    print("no new posts");
    exit();

}

//seems to always get 18 posts which are in chronological order, so start from the media id of the oldest post and see if weve posted it yet
$indexToStartFrom = 0;
if (!(0 == filesize("$folderPath/lastMediaIdScraped.txt"))){


    $indexToStartFrom = 0;
    foreach($scrapedItems as $scrapedItem){
        if (($scrapedItem->getId()) == $lastMediaIdScraped){
            $indexToStartFrom++;
            break;
        }
        else{
            $indexToStartFrom++;
        }
    }

}

print("index starting from : , $indexToStartFrom");
for($x = $indexToStartFrom; $x < count($scrapedItems); $x ++){
    $item = $scrapedItems[$x];

    $mediaId = $item->getId();
    $mediaType = $item->getMediaType();

    if($mediaType == 1){
       
        $path = "$folderPath/media/" . $x . ".jpg";
        imageDownload($path, $item);
      
      }
      if($mediaType == 2){
        
        $path = "$folderPath/media/" . $x . ".mp4";
        videoDownload($path, $item);
    
      }
      if($mediaType == 8){
        $carouselMedia = $item->getCarouselMedia();
        //create a folder to store all media
        $uniqueId = uniqid();
        mkdir($folderPath . "/media/" . $uniqueId);
        $filePath = $folderPath . "/media/$uniqueId/";


        $carouselCount = 0;
        foreach ($carouselMedia as $media) {
          $carouselCount++;
    
          $type = $media->getMediaType();
          if($type == 1){
            $path = $filePath . $carouselCount . ".jpg";
            imageDownload($path, $media);
          }
          if($type == 2){
            $path = $filePath . $carouselCount . ".mp4";
            videoDownload($path, $media);
          }
        }
      }









  $randMedia = randomMedia("$folderPath/media");
  $pathinfo = pathinfo($randMedia);


//its carousel
if(is_dir($randMedia)){
$media = [];
$files = [];

$fileSystemIterator = new FilesystemIterator($randMedia);

foreach($fileSystemIterator as $tempFile) {
    $mediaParts = pathinfo($tempFile);
    //whateverthe name is, -1 and that is its place in the array
    //we do this so media is uploaded in order
    $fileNumber = $mediaParts["filename"];
    $files[$fileNumber - 1] = $tempFile;

}
//keysort array and now all media will be in order
ksort($files);

$count = -1;
foreach($files as $file) {

  $count++;
  $mediaParts = pathinfo($file);
  if ($mediaParts["extension"] == "jpg"){
    $media[$count]['type'] = 'photo';
    $media[$count]['file'] = $file;
  }
  if ($mediaParts["extension"] == "mp4"){
    $media[$count]['type'] = 'video';
    $media[$count]['file'] = $randMedia . "/" . $mediaParts["basename"];
  }
}



//ORDER FILES BY CRAETION TIME ONDISK?????

foreach ($media as &$item) {
    /** @var \InstagramAPI\Media\InstagramMedia|null $validMedia */
    $validMedia = null;
    switch ($item['type']) {
        case 'photo':
            $validMedia = new \InstagramAPI\Media\Photo\InstagramPhoto($item['file'], $mediaOptions);
            break;
        case 'video':
            $validMedia = new \InstagramAPI\Media\Video\InstagramVideo($item['file'], $mediaOptions);
            break;
        default:
            // Ignore unknown media type.
    }
    if ($validMedia === null) {
        continue;
    }
    try {
        $item['file'] = $validMedia->getFile();
        // We must prevent the InstagramMedia object from destructing too early,
        // because the media class auto-deletes the processed file during their
        // destructor's cleanup (so we wouldn't be able to upload those files).
        $item['__media'] = $validMedia; // Save object in an unused array key.
    } catch (\Exception $e) {
        continue;
    }
    if (!isset($mediaOptions['forceAspectRatio'])) {
        // Use the first media file's aspect ratio for all subsequent files.
        /** @var \InstagramAPI\Media\MediaDetails $mediaDetails */
        $mediaDetails = $validMedia instanceof \InstagramAPI\Media\Photo\InstagramPhoto
            ? new \InstagramAPI\Media\Photo\PhotoDetails($item['file'])
            : new \InstagramAPI\Media\Video\VideoDetails($item['file']);
        $mediaOptions['forceAspectRatio'] = $mediaDetails->getAspectRatio();
    }
}
unset($item);
/////////////////////////////
try {
    $ig->timeline->uploadAlbum($media, ['caption' => '']);
} catch (\Exception $e) {
    echo 'Something went wrong: '.$e->getMessage()."\n";
}

array_map('unlink', glob("$randMedia/*.*"));
rmdir($randMedia);

//end of carousel upload!
}
else{
  //video or image
  $mediaParts = pathinfo($randMedia);

  if($mediaParts["extension"] == "jpg" || $mediaParts["extension"] == "png"){

    try {
      //if theres more problems with cropping try the old technique and dont use this \instagram photo thing - ur answer is on stack
        $photo = new \InstagramAPI\Media\Photo\InstagramPhoto($randMedia);
        $ig->timeline->uploadPhoto($photo->getFile(), ['caption' => '']);
    } catch (\Exception $e) {
        echo 'Something went wrong: '.$e->getMessage()."\n";
    }
    //delete uploaded media
    unlink($randMedia);

  }

  if ($mediaParts["extension"] == "mp4") {

try {
    $video = new \InstagramAPI\Media\Video\InstagramVideo($randMedia);
    $ig->timeline->uploadVideo($video->getFile(), ['caption' => '']);
} catch (\Exception $e) {
    echo 'Something went wrong: '.$e->getMessage()."\n";
    }

    //delete uploaded media
    unlink($randMedia);


  }
}


    }




//write new last media id
$newLastMediaIdScraped = (end($scrapedItems))->getId();
file_put_contents("$folderPath/lastMediaIdScraped.txt", $newLastMediaIdScraped);

