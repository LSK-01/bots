<?php

set_time_limit(0);
date_default_timezone_set('UTC');
$folderPath = "/Users/luca/Desktop/bots/trevorbot/saladSnakeScraper";
require "/Users/luca/Desktop/bots/trevorbot/composer/vendor/autoload.php";


/////// CONFIG ///////
$username = 'meem.scraper.6969';
$password = 'trevorbot4000';
$mediaIdArray = [];
$debug = false;
$truncatedDebug = false;
//////////////////////

//get a random file/folder
function randomMedia($path)
{
    $files = glob($path . '/*');
    $file = array_rand($files);
    return $files[$file];
}

function fetchCaption($path)
{
  $read = fopen($path, "r");
  $op = fread($read, filesize($path));
  fclose($read);
  return $op;
}


$ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);
//for making sure video format/dimensions etc. are compatible to upload to insta
\InstagramAPI\Utils::$ffprobeBin = '/usr/local/bin/ffprobe';
\InstagramAPI\Media\Video\FFmpeg::$defaultBinary = '/usr/local/bin/ffmpeg';

try {
    $ig->login($username, $password);
} catch (\Exception $e) {
    echo 'Something went wrong: '.$e->getMessage()."\n";
    exit();
}

//if there is no more media to upload delete any remaining files
if(count(glob("$folderPath/media/*.{jpg,mp4}", GLOB_BRACE)) == 0){
  array_map('unlink', glob( "$folderPath/media/*.txt"));
}


if(count(glob("$folderPath/media/*")) === 0){

  echo "getting more media to post";
  $output2 = shell_exec("/usr/local/bin/python3 $folderPath/getPicsReddit.py");
  $output1 = shell_exec("/usr/bin/php $folderPath/getPicsInstagram.php");
  
  file_put_contents("$folderPath/debug/getPicsInstagram.txt", $output1);
  file_put_contents("$folderPath/debug/getPicsReddit.txt", $output2);
  //$output2 = shell_exec("python3 getPicsReddit.py");

  if(count(glob("media/*")) === 0){
    echo "\n\n like/upvote more media - no more media to fetch or post";
    exit();
  }
}

//sometimes chooses .txt so we need to fix that
do{
  $randMedia = randomMedia("$folderPath/media");
  $pathinfo = pathinfo($randMedia);

}while($pathinfo["extension"] === "txt");

//its carousel
if(is_dir($randMedia)){
$media = [];
$files = [];

$fileSystemIterator = new FilesystemIterator($randMedia);

foreach($fileSystemIterator as $tempFile) {
    $mediaParts = pathinfo($tempFile);
    //whateverthe name is, -1 and that is its place in the array
    //we do this so media is uploaded in order
    $fileNumber = $mediaParts["filename"];
    $files[$fileNumber - 1] = $tempFile;

}
//keysort array and now all media will be in order
ksort($files);

$count = -1;
foreach($files as $file) {

  $count++;
  $mediaParts = pathinfo($file);
  if ($mediaParts["extension"] == "jpg"){
    $media[$count]['type'] = 'photo';
    $media[$count]['file'] = $file;
  }
  if ($mediaParts["extension"] == "mp4"){
    $media[$count]['type'] = 'video';
    $media[$count]['file'] = $randMedia . "/" . $mediaParts["basename"];
  }
}

$captionPath = $randMedia . "/caption.txt";
$captionText = file_get_contents($captionPath, true);
$caption = "$captionText";


////// NORMALIZE MEDIA //////
// All album files must have the same aspect ratio.
// We copy the app's behavior by using the first file
// as template for all subsequent ones.
$mediaOptions = [
    'targetFeed' => \InstagramAPI\Constants::FEED_TIMELINE_ALBUM,
    // Uncomment to expand media instead of cropping it.
    'operation' => \InstagramAPI\Media\InstagramMedia::EXPAND,
];

//ORDER FILES BY CRAETION TIME ONDISK?????

foreach ($media as &$item) {
    /** @var \InstagramAPI\Media\InstagramMedia|null $validMedia */
    $validMedia = null;
    switch ($item['type']) {
        case 'photo':
            $validMedia = new \InstagramAPI\Media\Photo\InstagramPhoto($item['file'], $mediaOptions);
            break;
        case 'video':
            $validMedia = new \InstagramAPI\Media\Video\InstagramVideo($item['file'], $mediaOptions);
            break;
        default:
            // Ignore unknown media type.
    }
    if ($validMedia === null) {
        continue;
    }
    try {
        $item['file'] = $validMedia->getFile();
        // We must prevent the InstagramMedia object from destructing too early,
        // because the media class auto-deletes the processed file during their
        // destructor's cleanup (so we wouldn't be able to upload those files).
        $item['__media'] = $validMedia; // Save object in an unused array key.
    } catch (\Exception $e) {
        continue;
    }
    if (!isset($mediaOptions['forceAspectRatio'])) {
        // Use the first media file's aspect ratio for all subsequent files.
        /** @var \InstagramAPI\Media\MediaDetails $mediaDetails */
        $mediaDetails = $validMedia instanceof \InstagramAPI\Media\Photo\InstagramPhoto
            ? new \InstagramAPI\Media\Photo\PhotoDetails($item['file'])
            : new \InstagramAPI\Media\Video\VideoDetails($item['file']);
        $mediaOptions['forceAspectRatio'] = $mediaDetails->getAspectRatio();
    }
}
unset($item);
/////////////////////////////
try {
    $ig->timeline->uploadAlbum($media, ['caption' => $caption]);
} catch (\Exception $e) {
    echo 'Something went wrong: '.$e->getMessage()."\n";
}

array_map('unlink', glob("$randMedia/*.*"));
rmdir($randMedia);

//end of carousel upload!
}
else{
  //video or image
  $mediaParts = pathinfo($randMedia);

  if($mediaParts["extension"] == "jpg" || $mediaParts["extension"] == "png"){
    $captionPath = "$folderPath/media/caption" . $mediaParts["filename"] . ".txt";
    $captionText = file_get_contents($captionPath, true);
    $caption = "$captionText";

    try {
      //if theres more problems with cropping try the old technique and dont use this \instagram photo thing - ur answer is on stack
        $photo = new \InstagramAPI\Media\Photo\InstagramPhoto($randMedia);
        $ig->timeline->uploadPhoto($photo->getFile(), ['caption' => $caption]);
    } catch (\Exception $e) {
        echo 'Something went wrong: '.$e->getMessage()."\n";
    }
    //delete uploaded media
    unlink($randMedia);
    print("unlinking files");
    unlink($captionPath);
  }

  if ($mediaParts["extension"] == "mp4") {
    $captionPath = "$folderPath/media/caption" . $mediaParts["filename"] . ".txt";
    $captionText = file_get_contents($captionPath, true);
    $caption = "$captionText - volume up.";

try {
    $video = new \InstagramAPI\Media\Video\InstagramVideo($randMedia);
    $ig->timeline->uploadVideo($video->getFile(), ['caption' => $caption]);
} catch (\Exception $e) {
    echo 'Something went wrong: '.$e->getMessage()."\n";
    }

    //delete uploaded media
    unlink($randMedia);
    unlink($captionPath);

  }
}

//comment spam cus shitty insta algorithm
$feed = $ig->timeline->getSelfUserFeed();
$items = $feed->getItems();

$latest = $items[0]->getId();
/*
for($x = 0; $x<5; $x ++){
sleep(5);
$ig->media->comment($latest, ".");
}

//reply to any comments on previous post
$lastPost = $items[1]->getId();
$comments = $ig->media->getComments($lastPost);
print($comments);
*/
