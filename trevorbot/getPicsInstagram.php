<?php
// INFO
//media_type1 is an image
//media_type2 is a video
//media_type8 is a carousel

function imageDownload($imagePath, $mediaObj){
  $url = $mediaObj->getImageVersions2()->getCandidates()[0]->getUrl();
  $data = file_get_contents($url);
  file_put_contents($imagePath, $data);
}

function videoDownload($videoPath, $mediaObj){
  $url = $mediaObj->getVideoVersions()[0]->getUrl();
  $data = file_get_contents($url);
  file_put_contents($videoPath, $data);
}

function getTrueOp($item){
  //check if they are @ting someone in the caption and add them to credits
  if ($item->getCaption() != null){
    $opCaption = $item->getCaption()->getText();
    print($opCaption);
    $opCaptionExploded = explode(" ", $opCaption);
    //if the @ sign is not by itself
    if (!in_array("@", $opCaptionExploded)){
      print("not in array");
      //so now if the caption contains @ we know it is not by itself
      if (strpos($opCaption, "@") !== false){
        print("there is a credit in caption");
        //get first string of @ appearance
        foreach ($opCaptionExploded as $value) {
          if (strpos($value, "@") !== false){
            print("not equal to false");
            $trueOp = $value;
             break;
          }
        }
        $trueOp = str_replace("@","",$trueOp);
        return($trueOp);
      }
    }
  }
}

set_time_limit(0);
date_default_timezone_set('UTC');
$workingDirectory = "/Users/luca/Desktop/bots/trevorbot";
require "$workingDirectory/composer/vendor/autoload.php";

/////// CONFIG ///////
$username = 'trevor.bot';
$password = 'acidifiedPotassiumManganate';
$mediaIdArray = [];
$debug = false;
$truncatedDebug = false;
//////////////////////

$ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);
try {
    $ig->login($username, $password);
} catch (\Exception $e) {
    echo 'Something went wrong: '.$e->getMessage()."\n";
    exit();
}

//1 - get last page of liked images

$nextMaxId = "";
$prevMaxId = "";

//need to fix this
/*
do{
  $prevMaxId = $nextMaxId;
  //feed is null
  print("feed $feed");
  $feed = $ig->media->getLikedFeed($nextMaxId);
  $nextMaxId = $feed->getNextMaxId();
}while($nextMaxId != "");
*/

//this is a temporary solution - just getting the first page. i want to get the oldest memes not the newest
$feed = $ig->media->getLikedFeed();
print("the feed.........$feed");


//2 - download images/videos from the last page
$items = $feed->getItems();
print("GOT TE TEN ITEMS: $items");

//i think it glitches out with pagination and it will have an empty nextMaxId for some reason
/*
if ($items == null){
  print("ITEMS IS NULL RIP - $items, getting the previous page");
  print("$prevMaxId - $nextMaxId");
  $feed = $ig->media->getLikedFeed($prevMaxId);
  $items = $feed->getItems();

  //if items is still null then for the life of me just break
  if ($items == null){
    print("ITEMS IS STILL NULL - $items, exiting");
    exit();
  }
  else{
    print("items not null yay -  $items");
  }
};*/

/*
$maxId = null;
$usernamesAllowed = [];
try{
  $rankToken = \InstagramAPI\Signatures::generateUUID();
  do{
    $request = $ig->people->getFollowing(5866664751, $rankToken,null,$maxId);
    foreach($request->getUsers() as $user){

      print($user->getUsername());
      //push to usernamesallowed array
      array_push($usernamesAllowed, $user->getUsername());
    }
    $maxId = $request->getNextMaxId();
    //so we dont get throttled
    sleep(5);
   }
  while($maxId !== null);
}
catch (\Exception $e) {
  echo 'Something went wrong paginating usernames: '.$e->getMessage()."\n";
}
*/
$count = 0;
foreach ($items as $item) {

  //so we can unlike the images after
  $mediaId = $item->getId();
  array_push($mediaIdArray, $mediaId);

  //find media type - video, carousel, image
  $mediaType = $item->getMediaType();

  //get true op if there is one
  //$credit = getTrueOp($item);
  $credit = $item->getUser()->getUsername();



 // if(!(in_array($credit, $usernamesAllowed))){
   // print("skipping this index foreach loop - username $credit is not being followed");
  //}
//else{

  if($mediaType != 8){
    $count++;
    //create caption.txt
    file_put_contents($workingDirectory . "/media/caption" . $count . ".txt", "creds @" . $credit);

  }

  if($mediaType == 1){
    $path = "$workingDirectory/media/" . $count . ".jpg";
    imageDownload($path, $item);

  }
  if($mediaType == 2){
    $path = "$workingDirectory/media/" . $count . ".mp4";
    videoDownload($path, $item);
  }
  if($mediaType == 8){
    $carouselMedia = $item->getCarouselMedia();
    //create a folder to store all media
    $uniqueId = uniqid();
    mkdir($workingDirectory . "/media/" . $uniqueId);
    $filePath = $workingDirectory . "/media/$uniqueId/";

    //create caption.txt
    file_put_contents($filePath . "caption.txt", "creds @" . $credit);

    $carouselCount = 0;
    foreach ($carouselMedia as $media) {
      $carouselCount++;

      $type = $media->getMediaType();
      if($type == 1){
        $path = $filePath . $carouselCount . ".jpg";
        imageDownload($path, $media);
      }
      if($type == 2){
        $path = $filePath . $carouselCount . ".mp4";
        videoDownload($path, $media);
      }

    }
  }
//}

}

//unlike images
foreach ($mediaIdArray as $id) {
  print("unliking media");
  $ig->media->unlike($id);
  print($mediaIdArray);
}
